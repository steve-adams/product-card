import React from 'react';
import ReactDOM from 'react-dom';
import './assets/css/main.css';
import ProductListContainer from './containers/ProductListContainer';

declare let module: any;

ReactDOM.render(<ProductListContainer />, document.getElementById('card-demo'));

if (module.hot) {
  module.hot.accept();
}
