import { Product } from './typings';

import blueShirtSwatch from './assets/images/products/t-shirt/blue-swatch.jpg';
import blueShirt from './assets/images/products/t-shirt/blue.jpg';
import greyShirtSwatch from './assets/images/products/t-shirt/grey-swatch.jpg';
import greyShirt from './assets/images/products/t-shirt/grey.jpg';
import orangeShirtSwatch from './assets/images/products/t-shirt/orange-swatch.jpg';
import orangeShirt from './assets/images/products/t-shirt/orange.jpg';

const getProducts = (): Product[] => [
  {
    id: 1,
    name: 'Ultrafine Merino T-Shirt',
    url: 'https://shirts.com/ultrafine_merino_t-shirt',
    variants: [
      {
        id: 1,
        parentId: 1,
        name: 'Blue',
        description: 'A blue merino wool t-shirt',
        imageUrl: blueShirt,
        price: 80.0,
        rating: 4,
        stock: 1,
        swatchImageUrl: blueShirtSwatch
      },
      {
        id: 2,
        parentId: 1,
        name: 'Grey',
        description: 'A grey merino wool t-shirt',
        imageUrl: greyShirt,
        price: 70.5,
        rating: 5,
        stock: 3,
        swatchImageUrl: greyShirtSwatch
      },
      {
        id: 3,
        parentId: 1,
        name: 'Orange',
        description: 'An orange merino wool t-shirt',
        imageUrl: orangeShirt,
        price: 85.0,
        rating: 3,
        stock: 1,
        swatchImageUrl: orangeShirtSwatch
      }
    ]
  }
];

export default getProducts;
