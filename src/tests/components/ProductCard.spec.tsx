import React from 'react';
import { cleanup, fireEvent, render, screen } from '@testing-library/react';

import getProducts from '../../util';
import { OnSyntheticEvent, Product, ProductVariant } from '../../typings';
import ProductCard from '../../components/ProductCard';

const product: Product = getProducts()[0];

const handler: OnSyntheticEvent<ProductVariant> = jest.fn();

afterEach(cleanup);

describe('<ProductCard />', () => {
  test('Matches the snapshot', () => {
    const { asFragment } = render(
      <ProductCard
        buyHandler={handler}
        product={product}
        selected={product.variants[0]}
        setSelectedVariantHandler={handler}
      />
    );

    expect(asFragment()).toMatchSnapshot();
  });

  test('Outputs correct number of variants, correct variant information', async () => {
    render(
      <ProductCard
        buyHandler={handler}
        product={product}
        selected={product.variants[0]}
        setSelectedVariantHandler={handler}
      />
    );
    const swatches = await screen.findByTestId('product-card-swatches');

    expect(swatches.children.length).toEqual(
      Object.keys(product.variants).length
    );

    const variant = product.variants[0];
    const price = await screen.findByTestId('product-card-price');
    const swatch = await screen.findByTestId(
      `product-card-swatch-${product.id}-${variant.name}`
    );

    fireEvent.click(swatch);

    expect(handler).toHaveBeenCalledTimes(1);
    expect(price.innerHTML).toMatch(RegExp(`${variant.price}`));
  });
});
