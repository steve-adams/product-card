import React from 'react';
import { cleanup, render } from '@testing-library/react';

import getProducts from '../../util';
import { OnSyntheticEvent, Product, ProductVariant } from '../../typings';
import ProductList from '../../components/ProductList';

const product: Product = getProducts()[0];

const handler: OnSyntheticEvent<ProductVariant> = jest.fn();

afterEach(cleanup);

describe('<ProductList />', () => {
  test('Matches the snapshot', () => {
    const { asFragment } = render(
      <ProductList
        buyHandler={handler}
        products={[product]}
        selected={product.variants[0]}
        setSelectedVariantHandler={handler}
      />
    );

    expect(asFragment()).toMatchSnapshot();
  });
});
