import { SyntheticEvent } from 'react';

export type ProductVariant = {
  id: number;
  parentId: number;
  name: string;
  description: string;
  price: number;
  rating: number;
  imageUrl: string;
  swatchImageUrl: string;
  stock: number;
};

export type ProductVariants = ProductVariant[];

export type Product = {
  id: number;
  name: string;
  url: string;
  variants: ProductVariants;
};

/**
 * COMMON EVENT HANDLER TYPES
 */

export type OnSyntheticEvent<V> = (value: V, event: SyntheticEvent) => void;
