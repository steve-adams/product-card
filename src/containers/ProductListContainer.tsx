import React, { useState } from 'react';
import produce from 'immer';
import ProductList from '../components/ProductList';
import getProducts from '../util';
import { OnSyntheticEvent, ProductVariant } from '../typings';

const ProductListContainer: React.FC = () => {
  const productList = getProducts();
  const selectedVariant = productList[0].variants[0];

  const [products, setProducts] = useState(productList);
  const [selected, setSelected] = useState(selectedVariant);

  const buyHandler: OnSyntheticEvent<ProductVariant> = (product, event) => {
    event.preventDefault();

    const updatedVariant = { ...product, stock: product.stock - 1 };

    const updatedProducts = produce(products, draft => {
      const pIndex = draft.findIndex(p => p.id === updatedVariant.parentId);
      const vIndex = draft[pIndex].variants.findIndex(
        v => v.id === updatedVariant.id
      );

      draft[pIndex].variants[vIndex] = updatedVariant;

      return draft;
    });

    setProducts(updatedProducts);
    setSelected(updatedVariant);
  };

  const setSelectedHandler: OnSyntheticEvent<ProductVariant> = (
    product,
    event
  ) => {
    event.preventDefault();

    setSelected(product);
  };

  return (
    <ProductList
      products={products}
      selected={selected}
      buyHandler={buyHandler}
      setSelectedVariantHandler={setSelectedHandler}
    />
  );
};

export default ProductListContainer;
