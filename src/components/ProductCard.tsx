import React from 'react';
import StarRating from 'react-svg-star-rating';
import { Product, ProductVariant, OnSyntheticEvent } from '../typings';

type ProductCardProps = {
  product: Product;
  selected: ProductVariant;
  buyHandler: OnSyntheticEvent<ProductVariant>;
  setSelectedVariantHandler: OnSyntheticEvent<ProductVariant>;
};

const ProductCard: React.FC<ProductCardProps> = ({
  product,
  selected,
  buyHandler,
  setSelectedVariantHandler: setSelectedHandler
}: ProductCardProps): JSX.Element => (
  <div className={`card ${selected.stock > 0 ? '' : 'sold-out'}`}>
    <div className="image hide-child">
      <img src={selected.imageUrl} alt={selected.description} />
      {selected.stock ? (
        <div className="call-to-action child">
          <a
            href={product.url}
            className="button primary grow"
            onClick={(event): void => buyHandler(selected, event)}
          >
            Add to cart
          </a>
        </div>
      ) : (
        <div className="sold-out">
          Sold
          <br />
          Out
        </div>
      )}
    </div>
    <div className="swatches" data-testid="product-card-swatches">
      {product.variants.map(
        (variant: ProductVariant, index: number): JSX.Element => {
          return (
            <div
              key={`swatch-${variant.name}`}
              className={`swatch ${selected === variant ? 'selected' : ''}`}
              onClick={(event): void => setSelectedHandler(variant, event)}
              onKeyPress={(event): void => setSelectedHandler(variant, event)}
              role="button"
              tabIndex={index}
              data-testid={`product-card-swatch-${product.id}-${variant.name}`}
            >
              <img
                src={variant.swatchImageUrl}
                alt={`Swatch for item: ${variant.name}`}
              />
            </div>
          );
        }
      )}
    </div>

    <p className="name">{product.name}</p>
    <p data-testid="product-card-price">${selected.price}</p>

    <StarRating
      initialRating={selected.rating}
      activeColor="rgb(0, 122, 255)"
      hoverColor="rgba(0, 122, 255, 0.5)"
      size={25}
      innerRadius={20}
      outerRadius={40}
      isReadOnly
    />
  </div>
);

export default ProductCard;
