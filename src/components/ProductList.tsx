import React from 'react';
import ProductCard from './ProductCard';
import { Product, ProductVariant, OnSyntheticEvent } from '../typings';

export type Props = {
  products: Product[];
  selected: ProductVariant;
  buyHandler: OnSyntheticEvent<ProductVariant>;
  setSelectedVariantHandler: OnSyntheticEvent<ProductVariant>;
};

const ProductList: React.FC<Props> = ({
  products,
  selected,
  buyHandler,
  setSelectedVariantHandler
}: Props): JSX.Element => (
  <ul className="list flex justify-center">
    {products.map((product: Product) => (
      <li key={`product-${product.id}`} className="w-25 pa3 mr2">
        <ProductCard
          key={`product-card-${product.id}`}
          product={product}
          selected={selected}
          buyHandler={buyHandler}
          setSelectedVariantHandler={setSelectedVariantHandler}
        />
      </li>
    ))}
  </ul>
);

export default ProductList;
