# ProductCard

![alt text](https://dzwonsemrish7.cloudfront.net/items/0W311Y2h3B2035023T2m/Screen%20Recording%202018-08-24%20at%2012.16%20AM.gif 'An example of the product card in action')

A TypeScript, react-based component for displaying single or multiple products on cards. It offers support for features like star-based ratings, product variants (with individual prices, images, stocks, and descriptions), add-to-cart functionality, and sold-out states.

This is a playground to experiment with TypeScript and React tooling, and doesn't reflect a sensible toolchain for such a simple React component. The focus is mostly on the development dependencies; I wanted to learn which TypeScript configuration I'd like most for larger projects, how to configure a test runner, if Parcel is sufficient in place of WebPack, what are sane settings for eslint, and more.

## Installing

- `git clone git@bitbucket.org:steve-adams/product-card.git`
- `yarn`

## Running

`yarn start`

This will bundle the app with parcel and open a browser window with the live, hot-reloading code ready.

## Editing

After running the code, all changes will be reloaded via parcel's HMR tooling.

## Testing

`yarn test` will run the tests a single time. Use `yarn test --watch` to get live results as files change.
